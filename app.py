#!/usr/bin/env python
#
# Copyright 2017 Ilya Flakin
#
"""
A simple Telegram bot which convert text to audio with
(service "Yandex SpeechKit Cloud" https://tech.yandex.ru/speechkit/cloud/)
To run this app, you must first register an application with Yandex and register bot in telegramm:
  1) Yandex - https://tech.yandex.ru/speechkit/cloud/
  2) Telegram - https://core.telegram.org/bots/api
  3) Set environment variables:
  BOT_YANDEX_KEY - API key Yandex SpeechKit
  BOT_TELEGRAM_KEY -  Telegram's token
  MY_BOT_URL - URL where telegram will be sent requests https://core.telegram.org/bots/api#setwebhook
"""

import tornado.httpclient
import tornado.httputil
import urllib.parse
import json
import logging
import tornado.escape
import tornado.ioloop
import tornado.web
import os.path
import langid
from io import BytesIO
from tornado import gen
from tornado.options import define, options


logging.basicConfig(format=u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s',
                    level=logging.DEBUG)
define("port", default=8888, help="run on the given port", type=int)
define("debug", default=False, help="run in debug mode")
define("savefile", default=False, help="save audio file local")


API_YANDEX_KEY = os.getenv('BOT_YANDEX_KEY')
TELEGRAM_TOKEN = os.getenv('BOT_TELEGRAM_KEY')
TELEGRAM_URL = "https://api.telegram.org/bot%s/" % TELEGRAM_TOKEN
MY_URL = os.getenv('MY_BOT_URL')


class JsonHandler(tornado.web.RequestHandler):
    """Request handler where requests and responses speak JSON."""

    def prepare(self):
        # Incorporate request JSON into arguments dictionary.
        if self.request.body:
            try:
                json_data = tornado.escape.json_decode(self.request.body)
                self.request.arguments.update(json_data)
            except ValueError:
                message = 'Unable to parse JSON.'
                logging.error(message)
                self.send_error(400, message=message)  # Bad Request

        # Set up response dictionary.
        self.response = dict()

    def set_default_headers(self):
        self.set_header('Content-Type', 'application/json')

    def write_error(self, status_code, **kwargs):
        if 'message' not in kwargs:
            if status_code == 405:
                kwargs['message'] = 'Invalid HTTP method.'
            else:
                kwargs['message'] = 'Unknown error.'
        logging.error(kwargs['message'])
        self.response = kwargs
        self.write_json()

    def write_json(self):
        output = json.dumps(self.response)
        self.write(output)


def encode_multipart_formdata(fields, files):
    """
    This code from http://code.activestate.com/recipes/146306/
    fields is a sequence of (name, value) elements for regular form fields.
    files is a sequence of (name, filename, value) elements for data to be uploaded as files
    Return (content_type, body) ready for httplib.HTTP instance
    """
    BOUNDARY = '----------ThIs_Is_tHe_bouNdaRY_$'
    CRLF = '\r\n'
    L = []
    for (key, value) in fields:
        L.append('--' + BOUNDARY)
        L.append('Content-Disposition: form-data; name="%s"' % key)
        L.append('')
        L.append(value)
    for (key, filename, value) in files:
        L.append('--' + BOUNDARY)
        L.append('Content-Disposition: form-data; name="%s"; filename="%s"' % (key, filename))
        L.append('Content-Type: %s' % 'audio/mpeg')
        L.append('')
        L.append(value)
    L.append('--' + BOUNDARY + '--')
    L.append('')

    # Small section for binary data
    s = BytesIO()
    for element in L:
        s.write(element if type(element) == bytes else element.encode())
        s.write(CRLF.encode())
    body = s.getvalue()

    content_type = 'multipart/form-data; boundary=%s' % BOUNDARY
    return content_type, body


def check_lang(text):
    """
    function determines the language of the text
    :param text: text for determines the language
    """
    api_lang = {'ru': 'ru‑RU', 'en': 'en-US', 'tr': 'tr-TR', 'uk': 'uk-UK'}
    if langid.classify(text)[0] in api_lang:
        logging.info('Язык текста определен как: ' + langid.classify(text)[0])
        return api_lang[langid.classify(text)[0]]
    else:
        logging.warning('Язык текста определен как: ' + langid.classify(text)[0] + ', использоваться будет ru-RU')
        return api_lang['ru']

@gen.coroutine
def encode_text_to_audio(text, formataudio='mp3', speaker='zahar', emotion='good', speed=1):
    """
    The function encoding text in audio format
    :param text: Text that will be converted to audio file

    For params formataudio, lang, speaker, emotion, speed see a documentation of Yandex API Cloud Speech:
     https://tech.yandex.ru/speechkit/cloud/doc/dg/concepts/speechkit-dg-tts-docpage/
    """
    data = urllib.parse.urlencode({
        'text': text,
        'format': formataudio,
        'lang': check_lang(text),
        'speaker': speaker,
        'key': API_YANDEX_KEY,
        'emotion': emotion,
        'speed': speed
    })
    url = u"https://tts.voicetech.yandex.net/generate?"
    http_client = tornado.httpclient.AsyncHTTPClient()
    response = yield http_client.fetch(url + data)
    raise gen.Return(response)


@gen.coroutine
def send_audio_to_chat(result, chat_id, update_id):
    """
    The function sending audio to chat
    :param result: It contains data from the yandex server
    :param chat_id: Identifier of telegram chat where to send the file
    :param update_id: For naming local file
    :return:
    """
    http_client = tornado.httpclient.AsyncHTTPClient()
    data = {'chat_id': chat_id}
    if result.code != 200:
        data['text'] = 'Error occured, when runnig a query to Yandex API'
        logging.error('Error occured, when runnig a query to Yandex API. Error: ' + result.body)
        response = yield http_client.fetch(TELEGRAM_URL + 'sendMessage?' + urllib.parse.urlencode(data))
    else:
        if options.savefile:
            save_file(result, chat_id, update_id)
        content_type, body = encode_multipart_formdata([('chat_id', str(chat_id))], [('audio', 'audio.mp3', result.body)])
        headers = tornado.httputil.HTTPHeaders({"content-type": content_type, 'content-length': str(len(body))})
        response = yield http_client.fetch(TELEGRAM_URL + 'sendAudio', method='POST', headers=headers, body=body)
    if response.code == 200:
        logging.info('The file has been successfully sent')
    else:
        logging.error('An error occurred while sending file: ' + response.body)


def file_name_generator(chat, update_id):
    return str(chat) + "_" + str(update_id) + ".mp3"


def save_file(response, chat, update_id):
    """
    Saving audio to file with name = chat_id.mp3
    """
    name = file_name_generator(chat, update_id)
    with open('./audio/' + name, "wb") as f:
        f.write(response.body)
    f.close()
    logging.info("Saved file: " + name)


class MessageUpdatesHandler(JsonHandler):

    def send_quick_response(self, method='sendMessage', **kwargs):
        """
        Forming and sending a response to Telegram server
        :param method: see https://core.telegram.org/bots/api#available-methods
        """
        self.response['method'] = method
        self.response['chat_id'] = self.request.arguments['message']['chat']['id']
        self.response.update(kwargs)
        self.write_json()
        self.finish()

    @gen.coroutine
    def post(self):
        """
        Handler for request from Telegram servers
        """
        if 'message' in self.request.arguments:
            body_request = self.request.arguments['message']
            logging.info('Server received a new request')
            if body_request:
                command_text = body_request['text'].split('@')[0]
                logging.info('Message: ' + body_request['text'])
                if body_request.get('reply_to_message'):
                    if command_text == '':
                        self.send_quick_response(text='Where text?')
                    else:
                        self.send_quick_response(method='sendChatAction', action='Making_audio')
                        result_audio = yield encode_text_to_audio(body_request['text'])
                        send_audio_to_chat(result_audio, body_request['chat']['id'], self.request.arguments['update_id'])
                elif command_text == '/help':
                    self.send_quick_response(
                        text="Use /get command to convert your text, use /ping command to check bot status")
                elif command_text == '/ping':
                    self.send_quick_response(text='I\'m ready')
                elif command_text == '/get':
                    self.send_quick_response(text='Ok. Get me text', reply_to_message_id=body_request['message_id'],
                                             reply_markup={"force_reply": True})
                else:
                    self.send_quick_response(text="I don't know what you want. Use /help command.")


def check_env():
    # Checking environment variables
    if API_YANDEX_KEY is None or TELEGRAM_TOKEN is None or MY_URL is None:
        logging.fatal("Environment variable is not set!")
        return False
    else:
        return True


def set_webhook():
    # Setting Webhook see https://core.telegram.org/bots/api#setwebhook
    http_client = tornado.httpclient.HTTPClient()
    data = urllib.parse.urlencode({'url': MY_URL, 'allowed_updates': ["message"]})
    response = http_client.fetch(TELEGRAM_URL + 'setWebhook?' + data)
    if response.code != 200:
        logging.fatal("Can't set hook: %s. Quit." % response.text)
        return False
    else:
        logging.info("Webhook is already set, starting server...")
        return True


def main():
    if not check_env() or not set_webhook():
        exit(1)
    tornado.options.parse_command_line()
    # Starting server
    token = '/' + MY_URL.split('/')[-1]
    logging.info('Set token = ' + token)
    app = tornado.web.Application(
        [
            (token, MessageUpdatesHandler),
        ],
        debug=options.debug,
    )
    app.listen(options.port)
    logging.info("The server is running, waiting for requests...")
    tornado.ioloop.IOLoop.current().start()


if __name__ == "__main__":
    main()
