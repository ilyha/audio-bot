FROM python:3.6

RUN set -x && \
    mkdir /usr/bot && \
    mkdir /usr/bot/audio

COPY requirements.txt /tmp/

RUN pip install --no-cache-dir --disable-pip-version-check -r /tmp/requirements.txt

COPY . /usr/bot
WORKDIR /usr/bot

CMD ["python", "./app.py"]
